package services;

import javax.ejb.Remote;

import model.Personne;

@Remote
public interface PersonneServiceEJBRemote {
	/**
	 * This method add a person
	 * 
	 * @param personne
	 */
	public void addPerson(Personne personne);
	/**
	 * this method update a person
	 * @param personne
	 */
	public void updatePerson(Personne personne);
	/**
	 * this method remove a person
	 * @param personne
	 */
	public void removePerson(Personne personne);
	/**
	 * this method return a person by id
	 * @param id
	 * @return
	 */
	public Personne findPersonById(int id);
	
}
