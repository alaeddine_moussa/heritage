package services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Personne;

/**
 * Session Bean implementation class PersonneServiceEJB
 */
@Stateless
public class PersonneServiceEJB implements PersonneServiceEJBRemote {

	@PersistenceContext(unitName = "heritage")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public PersonneServiceEJB() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addPerson(Personne personne) {
		em.persist(personne);

	}

	@Override
	public void updatePerson(Personne personne) {
		em.merge(personne);

	}

	@Override
	public void removePerson(Personne personne) {
		em.remove(em.merge(personne));

	}

	@Override
	public Personne findPersonById(int id) {
		return em.find(Personne.class, id);
	}

}
