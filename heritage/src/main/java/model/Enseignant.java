package model;

import java.io.Serializable;
import javax.persistence.*;
import model.Personne;

/**
 * Entity implementation class for Entity: Enseignant
 *
 */
@Entity

public class Enseignant extends Personne implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Enseignant() {
		super();
	}
   
}
