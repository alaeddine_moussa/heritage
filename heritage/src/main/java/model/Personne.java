package model;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Personne
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@Access(AccessType.FIELD)
public class Personne implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idPersonne;
	private String email;
	private String nom;
	private static final long serialVersionUID = 1L;

	public Personne() {
		super();
	}   
	public int getIdPersonne() {
		return this.idPersonne;
	}

	public void setIdPersonne(int idPersonne) {
		this.idPersonne = idPersonne;
	} 
	@Access(AccessType.PROPERTY)
	@Column
	public String getEmail() {
		return this.email+"@";
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
   
}
