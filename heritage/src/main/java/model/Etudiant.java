package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import model.Personne;

/**
 * Entity implementation class for Entity: Etudiant
 *
 */
@Entity

public class Etudiant extends Personne  {

	@Basic(fetch=FetchType.LAZY)
	private byte[] photo;
	private Date dateNaissance;
	private static final long serialVersionUID = 1L;

	public Etudiant() {
		super();
	}   
	public byte[] getPhoto() {
		return this.photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}   
	public Date getDateNaissance() {
		return this.dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
   
}
